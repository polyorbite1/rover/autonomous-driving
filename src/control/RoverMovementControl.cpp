#include "autonomous_driving/control/RoverMovementControl.h"

void RoverMovementControl::execute(const ControlParameter& parameter)
{
    m_onExecuteMovement((const RoverMovementControlParameter&)parameter);
}