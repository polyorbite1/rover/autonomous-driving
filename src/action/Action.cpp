#include "autonomous_driving/action/Action.h"

void Action::executeOtherAction(const std::shared_ptr<Action>& action)
{
    onExecuteOtherAction(action);
}