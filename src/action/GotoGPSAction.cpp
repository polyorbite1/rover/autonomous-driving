#include "autonomous_driving/action/GotoGPSAction.h"

void GotoGPSAction::execute(const Perception& perception, const Actuation& actuation)
{
    GPSSense currentGPSFix = perception.gpsSense;
    GPSSense wantedGPSFix = m_wantedGPSFix;

    float latitude = wantedGPSFix.latitude - currentGPSFix.latitude;
    float longitude = wantedGPSFix.longitude - currentGPSFix.longitude;

    float wantedAngle = atan2(latitude, longitude);
    float wantedDistance = sqrt(pow(latitude, 2) + pow(longitude, 2));

    const float WANTED_DISTANCE_THRESHOLD = 0.01;
    if(wantedDistance < WANTED_DISTANCE_THRESHOLD)
    {
        onResolve();
    }

    executeOtherAction(std::make_shared<RotateToAngleAction>(0.1f, wantedAngle));
    executeOtherAction(std::make_shared<MoveForwardAction>(2.0f, 3000));
}
