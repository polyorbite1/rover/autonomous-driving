#include "autonomous_driving/action/RotateToAngleAction.h"

void RotateToAngleAction::execute(const Perception& perception, const Actuation& actuation)
{
    double currentAngle = perception.magneticSense.getYaw();
    double error = currentAngle - m_wantedAngle;
    bool rotationDirection = error >= 0;

    if(abs(error) > c_errorThreshold)
    {
        RoverMovementControlParameter wantedMovement;
        wantedMovement.angularVelocity = rotationDirection ? m_velocity : -m_velocity;
        wantedMovement.linearVelocity = 0.0f;

        actuation.move->execute(wantedMovement);
    }
    else
    {
        RoverMovementControlParameter wantedMovement;
        wantedMovement.angularVelocity = 0.0f;
        wantedMovement.linearVelocity = 0.0f;

        actuation.move->execute(wantedMovement);
        onResolve();
    }
}