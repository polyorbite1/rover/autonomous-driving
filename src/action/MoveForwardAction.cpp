#include "autonomous_driving/action/MoveForwardAction.h"

void MoveForwardAction::execute(const Perception& perception, const Actuation& actuation)
{
    if(m_isFirstUpdate)
    {
        m_isFirstUpdate = false;
        m_startTime = std::chrono::system_clock::now();
    }

    auto currentTime = std::chrono::system_clock::now();
    auto currentDuration = currentTime - m_startTime;

    if(currentDuration >= m_wantedDuration)
    {
        RoverMovementControlParameter wantedMovement;
        wantedMovement.angularVelocity = 0.0f;
        wantedMovement.linearVelocity = 0.0f;

        actuation.move->execute(wantedMovement);
        
        onResolve();
    }
    else
    {
        RoverMovementControlParameter wantedMovement;
        wantedMovement.angularVelocity = 0.0f;
        wantedMovement.linearVelocity = m_velocity;

        actuation.move->execute(wantedMovement);
    }
}