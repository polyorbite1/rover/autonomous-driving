#include "autonomous_driving/action/RotateToRight.h"
#include <unistd.h>
#include <chrono> 

void RotateToRight::execute(const Perception& perception, const Actuation& actuation)
{

        RoverMovementControlParameter wantedMovement;
        float angular_velocity = 0.1f;
        double time = m_wantedAngle / angular_velocity;

        auto start = std::chrono::high_resolution_clock::now();
        auto finish = std::chrono::high_resolution_clock::now();
        auto time_passed = finish - start;
        double time_passed_double = std::chrono::duration<double>(time_passed).count();

        wantedMovement.angularVelocity = angular_velocity;
        wantedMovement.linearVelocity = 0.0f;

        while (time_passed_double < time)
        {
                actuation.move->execute(wantedMovement);
                time_passed = finish - start;
                time_passed_double = std::chrono::duration<double>(time_passed).count();
        }

        // while (angular_velocity < 0.1f && angular_velocity > 0.4f)
        // {
        //         if (angular_velocity <= 0.1f) {
        //                 time = time - 100;
        //         } else {
        //                 time = time + 100;  
        //         };
        //         angular_velocity = m_wantedAngle / time;
        // }               
        // sleep(3);
        onResolve();

}