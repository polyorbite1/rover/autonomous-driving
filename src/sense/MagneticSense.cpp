#include "autonomous_driving/sense/MagneticSense.h"

double MagneticSense::getYaw() const
{
    return atan2(y, x);
}