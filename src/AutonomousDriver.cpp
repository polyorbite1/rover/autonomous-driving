#include "autonomous_driving/AutonomousDriver.h"

void AutonomousDriver::runStrategy(const std::string& name)
{
    bool strategyExists = m_strategies.find(name) != m_strategies.end();
    if(strategyExists)
    {
        m_currentStrategy = m_strategies.find(name)->second;
        m_actionsToExecute = std::stack<std::shared_ptr<Action>>();
    }
}

void AutonomousDriver::update(const Perception& perception)
{
    bool isRunningAStrategy = m_currentStrategy != nullptr;
    if(!isRunningAStrategy) return;

    bool isRunningAnAction = !m_actionsToExecute.empty();
    if(isRunningAnAction)
    {
        getCurrentAction()->execute(perception, m_actuation);
    }
    else
    {
        auto actionsToExecute = m_currentStrategy->execute(perception);
        while(!actionsToExecute.empty())
        {
            addActionToExecute(actionsToExecute.top());
            actionsToExecute.pop();
        }
    }
}

void AutonomousDriver::bindEventsToStrategy(const std::shared_ptr<Strategy>& strategy)
{
    strategy->onTerminate = [&]() {
        m_currentStrategy = nullptr;
    };
}

void AutonomousDriver::bindEventsToAction(const std::shared_ptr<Action>& action)
{
    action->onExecuteOtherAction = [&](const std::shared_ptr<Action>& action) {
        addActionToExecute(action);
    };

    action->onResolve = [&]() {
        m_actionsToExecute.pop();
    };

    action->onFail = [&](const std::string& message) {
        m_currentStrategy->onActionFailed(action, message);
        m_actionsToExecute.pop();
    };
}

void AutonomousDriver::addActionToExecute(const std::shared_ptr<Action>& action)
{
    bindEventsToAction(action);
    //m_actionsToExecute.push_front(action);
}

std::shared_ptr<Action> AutonomousDriver::getCurrentAction()
{
    return m_actionsToExecute.top();
}