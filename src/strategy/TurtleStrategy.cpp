#include "autonomous_driving/strategy/TurtleStrategy.h"

std::stack<std::shared_ptr<Action>> TurtleStrategy::execute(const Perception& senses)
{
    if(m_shouldTerminate)
    {
        terminate();
    }

    std::stack<std::shared_ptr<Action>> actionsToExecute;

    actionsToExecute.push(std::make_shared<RotateToAngleAction>(0.1f, -0.5*M_PI));
    actionsToExecute.push(std::make_shared<MoveForwardAction>(1.0f, 3000));
    actionsToExecute.push(std::make_shared<RotateToAngleAction>(0.1f, M_PI / 2));
    actionsToExecute.push(std::make_shared<MoveForwardAction>(-1.0f, 3000));
    actionsToExecute.push(std::make_shared<RotateToAngleAction>(0.1f, 0));

    m_shouldTerminate = true;

    return actionsToExecute;
}

void TurtleStrategy::onActionFailed(const std::shared_ptr<Action>& action, const std::string& message)
{
    
}
