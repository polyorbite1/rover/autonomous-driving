#include "autonomous_driving/strategy/GotoGPSStrategy.h"

std::stack<std::shared_ptr<Action>> GotoGPSStrategy::execute(const Perception& senses)
{
    if(m_shouldTerminate)
    {
        terminate();
    }

    std::stack<std::shared_ptr<Action>> actionsToExecute;

    GPSSense wantedGPSFix;
    wantedGPSFix.longitude = 1.2f;
    wantedGPSFix.latitude = 1.2f;
    actionsToExecute.push(std::make_shared<GotoGPSAction>(wantedGPSFix));

    m_shouldTerminate = true;

    return actionsToExecute;
}

void GotoGPSStrategy::onActionFailed(const std::shared_ptr<Action>& action, const std::string& message)
{
    
}