#include "autonomous_driving/strategy/ArucoDetectStrategy.h"
#include "autonomous_driving/action/RotateToRight.h"

#include <math.h> // TODO: deplace this 

std::stack<std::shared_ptr<Action>> ArucoDetectStrategy::execute(const Perception& senses)
{
    if(m_shouldTerminate)
    {
        terminate();
    }

    std::stack<std::shared_ptr<Action>> actionsToExecute;
    if (senses.arucoSense.id != -1) {
        actionsToExecute.push(std::make_shared<MoveForwardAction>(1.0f, 4000));
        m_shouldTerminate = true;
    } 
    else {
        actionsToExecute.push(std::make_shared<MoveForwardAction>(-1.0f, 1000));
    }

    // TODO: make work this code
    // if (senses.arucoSense.id != -1) {
    //     double rotation_angle = atan2(senses.arucoSense.translation_z, senses.arucoSense.translation_x);
    //     double distance = sqrt(pow(senses.arucoSense.translation_x, 2) + pow(senses.arucoSense.translation_y, 2) + pow(senses.arucoSense.translation_z, 2));
    //     actionsToExecute.push(std::make_shared<MoveForwardAction>(1.0f, 3000));
    //     // actionsToExecute.push(std::make_shared<RotateToAngleAction>(0.1f, rotation_angle));
    //     actionsToExecute.push(std::make_shared<RotateToRight>(M_PI / 2));
    //     while (distance >= 1)
    //     {
    //         actionsToExecute.push(std::make_shared<MoveForwardAction>(1.0f, 3000));
    //     }
        
    //     m_shouldTerminate = true;
    //     // if (senses.arucoSense.id == 100) {
    //     //     actionsToExecute.push(std::make_shared<MoveForwardAction>(1.0f, 6000));
    //     //     m_shouldTerminate = true;
    //     // }
    // } 
    // else {
    //     actionsToExecute.push(std::make_shared<MoveForwardAction>(1.0f, 3000));
    //     actionsToExecute.push(std::make_shared<RotateToRight>(M_PI / 2));
    // }

    // actionsToExecute.push(std::make_shared<MoveForwardAction>(-1.0f, 3000));
    // actionsToExecute.push(std::make_shared<RotateToAngleAction>(0.1f, 0));


    return actionsToExecute;
}

void ArucoDetectStrategy::onActionFailed(const std::shared_ptr<Action>& action, const std::string& message)
{
    
}