#include "autonomous_driving/strategy/IdleStrategy.h"

std::stack<std::shared_ptr<Action>> IdleStrategy::execute(const Perception& senses)
{
    std::stack<std::shared_ptr<Action>> actionsToExecute;
    actionsToExecute.push(std::make_shared<NoneAction>());
    return actionsToExecute;
}

void IdleStrategy::onActionFailed(const std::shared_ptr<Action>& action, const std::string& message)
{
    
}