#pragma once

#include "autonomous_driving/control/parameter/ControlParameter.h"

struct RoverMovementControlParameter : public ControlParameter
{
    float linearVelocity;
    float angularVelocity;
};