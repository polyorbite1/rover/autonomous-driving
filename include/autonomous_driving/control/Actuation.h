#pragma once

#include <memory>

#include "autonomous_driving/control/Control.h"
#include "autonomous_driving/control/RoverMovementControl.h"

class Actuation
{
public:
    std::shared_ptr<RoverMovementControl> move;
};