#pragma once

#include <functional>

#include "autonomous_driving/control/Control.h"
#include "autonomous_driving/control/parameter/RoverMovementControlParameter.h"

class RoverMovementControl : public Control
{
public:
    RoverMovementControl(const std::function<void(const RoverMovementControlParameter&)>& onExecuteMovement) : m_onExecuteMovement(onExecuteMovement) {}

    void execute(const ControlParameter& parameter) override;

private:
    std::function<void(const RoverMovementControlParameter&)> m_onExecuteMovement;

};