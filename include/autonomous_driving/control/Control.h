#pragma once

#include "autonomous_driving/control/parameter/ControlParameter.h"

class Control
{
public:
    virtual void execute(const ControlParameter& parameter) = 0;
};