#pragma once

#include "autonomous_driving/strategy/Strategy.h"
#include "autonomous_driving/action/NoneAction.h"

class IdleStrategy : public Strategy
{
public:
    std::stack<std::shared_ptr<Action>> execute(const Perception& senses) override;
    void onActionFailed(const std::shared_ptr<Action>& action, const std::string& message) override;

};