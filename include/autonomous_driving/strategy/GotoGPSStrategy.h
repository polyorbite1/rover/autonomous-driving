#pragma once

#include "autonomous_driving/strategy/Strategy.h"
#include "autonomous_driving/action/GotoGPSAction.h"
#include "autonomous_driving/sense/GPSSense.h"

using namespace std::chrono_literals;

class GotoGPSStrategy : public Strategy
{
public:
    std::stack<std::shared_ptr<Action>> execute(const Perception& senses) override;
    void onActionFailed(const std::shared_ptr<Action>& action, const std::string& message) override;

private:
    bool m_shouldTerminate = false;

};