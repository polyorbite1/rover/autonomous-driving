#pragma once

#include <memory>
#include <stack>
#include <functional>

#include "autonomous_driving/sense/Perception.h"
#include "autonomous_driving/action/Action.h"
#include "autonomous_driving/sense/Sense.h"

class Strategy
{
public:
    virtual std::stack<std::shared_ptr<Action>> execute(const Perception& senses) = 0;
    virtual void onActionFailed(const std::shared_ptr<Action>& action, const std::string& message) = 0;

    std::function<void(void)> onTerminate;

protected:
    void terminate();

};