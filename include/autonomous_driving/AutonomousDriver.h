#pragma once

#include <memory>
#include <map>
#include <list>
#include <stack>

#include "autonomous_driving/strategy/Strategy.h"
#include "autonomous_driving/strategy/IdleStrategy.h"
#include "autonomous_driving/strategy/TurtleStrategy.h"
#include "autonomous_driving/strategy/ArucoDetectStrategy.h"
#include "autonomous_driving/action/Action.h"
#include "autonomous_driving/control/Control.h"
#include "autonomous_driving/control/Actuation.h"

class AutonomousDriver
{
public:
    AutonomousDriver(const Actuation& actuation) : m_actuation(actuation) {
        // TODO: add your new strategy here like follows
        //   m_strategies["your_strategy"] = std::make_shared<YourStrategy>();

        m_strategies["aruco_detect"] = std::make_shared<ArucoDetectStrategy>();
        m_strategies["turtle"] = std::make_shared<TurtleStrategy>();
        m_strategies["idle"] = std::make_shared<IdleStrategy>();

        runStrategy("aruco_detect");

        for(auto strategyEntry : m_strategies)
        {
            auto strategy = strategyEntry.second;
            bindEventsToStrategy(strategy);
        }
    }

    void runStrategy(const std::string& name);
    void update(const Perception& perception);

private:
    void bindEventsToStrategy(const std::shared_ptr<Strategy>& strategy);

    void bindEventsToAction(const std::shared_ptr<Action>& action);
    void addActionToExecute(const std::shared_ptr<Action>& action);
    std::shared_ptr<Action> getCurrentAction();

private:
    std::shared_ptr<Strategy> m_currentStrategy;
    std::map<std::string, std::shared_ptr<Strategy>> m_strategies;

    std::stack<std::shared_ptr<Action>> m_actionsToExecute;

    Actuation m_actuation;

};