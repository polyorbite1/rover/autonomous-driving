#pragma once

#include <math.h>

#include "autonomous_driving/sense/Sense.h"

struct MagneticSense : public Sense
{
    double x;
    double y;
    double z;

    double getYaw() const;
};