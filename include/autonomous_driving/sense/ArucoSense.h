#pragma once

#include <math.h>

#include "autonomous_driving/sense/Sense.h"

struct ArucoSense : public Sense
{
    int32_t id = -1;
    double translation_x;
    double translation_y;
    double translation_z;
};