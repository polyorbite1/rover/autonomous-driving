#pragma once

#include <map>
#include <memory>
#include <string>

#include "autonomous_driving/sense/GPSSense.h"
#include "autonomous_driving/sense/MagneticSense.h"
#include "autonomous_driving/sense/ArucoSense.h"

struct Perception
{
    GPSSense gpsSense;
    MagneticSense magneticSense;
    ArucoSense arucoSense;
};
