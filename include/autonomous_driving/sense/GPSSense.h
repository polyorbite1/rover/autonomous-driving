#pragma once

#include "autonomous_driving/sense/Sense.h"

struct GPSSense : public Sense
{
    double latitude;
    double longitude;
    double altitude;
};