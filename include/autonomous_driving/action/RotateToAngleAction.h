#pragma once

#include <math.h>

#include "autonomous_driving/action/Action.h"
#include "autonomous_driving/control/parameter/RoverMovementControlParameter.h"

class RotateToAngleAction : public Action
{
public:
    RotateToAngleAction(
        const float& velocity,
        const double& wantedAngle
    ) :
        m_velocity(velocity),
        m_wantedAngle(wantedAngle - 2*M_PI*((int)(wantedAngle/(2*M_PI)))) // idiot-proof rotation angle
    {}

    void execute(const Perception& perception, const Actuation& actuation) override;

private:
    float m_velocity;
    double m_wantedAngle;
    const double c_errorThreshold = M_PI * 0.01;

};
