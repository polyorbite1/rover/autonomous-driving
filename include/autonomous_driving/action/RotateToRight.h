#pragma once

#include <math.h>

#include "autonomous_driving/action/Action.h"
#include "autonomous_driving/control/parameter/RoverMovementControlParameter.h"

class RotateToRight : public Action
{
public:
    RotateToRight(
        const double& wantedAngle
    ) :
        m_wantedAngle(wantedAngle)
    {}

    void execute(const Perception& perception, const Actuation& actuation) override;

private:
    double m_wantedAngle;

};