#pragma once

#include <functional>

#include "autonomous_driving/sense/Perception.h"
#include "autonomous_driving/control/Actuation.h"

class Action
{
public:
    virtual void execute(const Perception& perception, const Actuation& actuation) = 0;

    std::function<void(const std::shared_ptr<Action>&)> onExecuteOtherAction;
    std::function<void(void)> onResolve;
    std::function<void(std::string)> onFail;

protected:
    void executeOtherAction(const std::shared_ptr<Action>& action);

};
