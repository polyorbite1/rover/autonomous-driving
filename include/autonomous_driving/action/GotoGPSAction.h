#pragma once

#include <cmath>

#include "autonomous_driving/action/Action.h"
#include "autonomous_driving/action/MoveForwardAction.h"
#include "autonomous_driving/action/RotateToAngleAction.h"
#include "autonomous_driving/sense/GPSSense.h"

class GotoGPSAction : public Action
{
public:
    GotoGPSAction(const GPSSense& wantedGPSFix) : m_wantedGPSFix(wantedGPSFix) {}

    void execute(const Perception& perception, const Actuation& actuation) override;

private:
    GPSSense m_wantedGPSFix;

};
