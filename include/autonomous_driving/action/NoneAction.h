#pragma once

#include "autonomous_driving/action/Action.h"

class NoneAction : public Action
{
public:
    void execute(const Perception& perception, const Actuation& actuation) override;
};