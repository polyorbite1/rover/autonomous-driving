#pragma once

#include <chrono>

#include "autonomous_driving/action/Action.h"
#include "autonomous_driving/control/parameter/RoverMovementControlParameter.h"

class MoveForwardAction : public Action
{
public:
    MoveForwardAction(
        const float& velocity,
        const int64_t& durationMiliseconds
    ) :
        m_velocity(velocity),
        m_wantedDuration(std::chrono::milliseconds{durationMiliseconds})
    {}

    void execute(const Perception& perception, const Actuation& actuation) override;

private:
    std::chrono::system_clock::time_point m_startTime;
    std::chrono::duration<int64_t, std::milli> m_wantedDuration;

    float m_velocity;
    bool m_isFirstUpdate = true;

};